begin transaction;
update accounts set balance = 800 where id = 2;
update accounts set balance = -300 where id = 1;
update accounts set balance = 1100 where id = 3;
update accounts set balance = -400 where id = 1;
commit;